FROM golang:alpine as build_base
RUN apk add git
RUN mkdir /app 
ADD . /app/ 
WORKDIR /app 
RUN go mod download

FROM build_base as build_image
WORKDIR /app
RUN go build -o main cmd/k8s/main.go 

FROM alpine
RUN mkdir /app
COPY --from=build_image /app/main /app/main
EXPOSE 8080
ENV PORT 8080
CMD ["/app/main"]

