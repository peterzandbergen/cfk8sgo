# cfk8sgo

Go program to compare cf with k8s.

## Usage

Presents a rest interface on port 8080 or the value set by the PORT environment variable.

## Endpoints

- **/** => dumps the environment variables
- **/sleep/{seconds}** Sleeps {seconds} seconds before returning the 200 response
- **/crashme** Crashes the process
- **/load/{percentage}** Puts a load of percentage % on the available CPUs

