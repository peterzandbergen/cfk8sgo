package cfk8sgo

import (
	"expvar"
	"fmt"
	"log"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"time"

	"github.com/go-chi/chi"
)

type Server struct {
	r chi.Router

	loadDone chan struct{}
}

func (s *Server) Router() chi.Router {
	s.r = chi.NewRouter()

	s.r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		// create the default response, dump the environment.
		for _, e := range os.Environ() {
			fmt.Fprintf(w, "%s\n", e)
		}
	})

	s.r.Get("/crashme", func(w http.ResponseWriter, r *http.Request) {
		go func() {
			<-time.After(time.Second)
			os.Exit(1)
		}()
		log.Printf("crashing in 1 second")
		fmt.Fprintf(w, "crashing in 1 second")
	})

	s.r.Get("/sleep/{sleepmsec}", func(w http.ResponseWriter, r *http.Request) {
		// get parameter and convert to int
		var i int
		var err error
		if i, err = strconv.Atoi(chi.URLParam(r, "sleepmsec")); err != nil {
			log.Printf("error %s", err.Error())
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}
		time.Sleep(time.Duration(i) * time.Millisecond)
		fmt.Fprintf(w, "slept for %d msec\n", i)
	})

	s.r.Get("/load/{percentage}", func(w http.ResponseWriter, r *http.Request) {
		// Extract the parameter.
		var p int
		var err error
		if p, err = strconv.Atoi(chi.URLParam(r, "percentage")); err != nil {
			log.Printf("error %s", err.Error())
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}
		if s.SetLoad(p) {
			fmt.Fprintf(w, "load started %d percent \n ", p)
		} else {
			fmt.Fprintf(w, "load stopped")
		}
	})

	s.r.Get("/debug/vars", func(w http.ResponseWriter, r *http.Request) {
		expvar.Handler().ServeHTTP(w, r)
	})
	s.r.Get("/debug/vars", expvar.Handler().ServeHTTP)
	return s.r
}

func (s *Server) SetLoad(percentage int) bool {
	if percentage <= 0 {
		// Stop the load.
		if s.loadDone != nil {
			close(s.loadDone)
			s.loadDone = nil
		}
		return false
	}
	if s.loadDone != nil {
		// Already running, stop and start again.
		close(s.loadDone)
	}
	// Create the channel.
	s.loadDone = make(chan struct{})
	// Limit percentage
	if percentage > 99 {
		percentage = 99
	}
	// Determine the number of processors.
	n := runtime.NumCPU()
	log.Printf("num of process to start %d", n)
	// The load process.
	load := func(p int, done chan struct{}) {
		for {
			select {
			case <-done:
				return
			case <-time.After(1000 * time.Millisecond):
				// Be busy.
				stopTime := time.Now().Add(time.Duration(percentage*10) * time.Millisecond)
				for stopTime.After(time.Now()) {
				}
			}
		}
	}
	// Start the load processes.
	for i := 0; i < n; i++ {
		go load(percentage, s.loadDone)
	}
	return true
}
