package main

import (
	"testing"

	"gitlab.com/peterzandbergen/cfk8sgo"
)

func TestMain(t *testing.T) {
	var s *cfk8sgo.Server

	s = new(cfk8sgo.Server)

	s.Router()
	t.Log("main tested")
}
