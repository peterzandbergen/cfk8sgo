package main

import (
	"fmt"
	"net/http"
	"os"

	"gitlab.com/peterzandbergen/cfk8sgo"
)

func main() {
	var s *cfk8sgo.Server

	s = new(cfk8sgo.Server)

	port := os.Getenv("PORT")
	if len(port) <= 0 {
		port = "8080"
	}

	addr := ":" + port

	fmt.Println("listening on " + addr)
	http.ListenAndServe(addr, s.Router())
}
