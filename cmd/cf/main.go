package main

import (
	"net/http"

	"gitlab.com/peterzandbergen/cfk8sgo"
)

func main() {
	var s *cfk8sgo.Server

	s = new(cfk8sgo.Server)

	http.ListenAndServe(":8080", s.Router())
}
